#
# Be sure to run `pod lib lint CSUpgradeHelper.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CSUpgradeHelper'
  s.version          = '1.0.24'
  s.summary          = '组件库-升级'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
GO1 GO2项目，还有以后的项目都会有升级模块，且升级流程都很相似，故
                       DESC

  s.homepage         = 'https://gitlab.com/shangxiaobing/CSUpgradeHelper'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '尚小冰' => 'shalay@chasing-innovation.com' }
  s.source           = { :git => 'https://gitlab.com/shangxiaobing/CSUpgradeHelper.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'CSUpgradeHelper/Classes/**/*'
  
#   s.resource_bundles = {
#     'CSUpgradeHelper' => ['CSUpgradeHelper/Assets/*']
#   }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
   s.dependency 'CSGCDAsyncSocket'
   s.dependency 'AFNetworking/Reachability', '~> 3.2.1'
   s.dependency 'AFNetworking/Serialization', '~> 3.2.1'
   s.dependency 'AFNetworking/Security', '~> 3.2.1'
   s.dependency 'AFNetworking/NSURLSession', '~> 3.2.1'
   
end
